import Todo from './Todo'
import { ref, Ref } from 'vue'

class TodosList {
    readonly ref = ref<Todo[]>([])

    getSorted(): Todo[] {
        return this.ref.value.sort((todo1, todo2) => todo2.creationDate - todo1.creationDate)
    }
    includesByContent(content: string): boolean {
        return Boolean(this.ref.value.find(todo => todo.content === content))
    }
    push(todo: Todo): void {
        this.ref.value.push(todo)
        return
    }
    remove(todo: Todo): void {
        this.ref.value = this.ref.value.filter((t) => t.creationDate !== todo.creationDate)
        return
    }
    set(jsonStringified: string|null): void {
        this.ref.value = jsonStringified ? JSON.parse(jsonStringified) : []
        return
    }
    removeMethod = (todo: Todo) => this.remove(todo)
    addMethod = (contentRef: Ref<string>) => () => {
        if (contentRef.value.trim() === '' || this.includesByContent(contentRef.value)) {
            return
        }
    
        this.push(new Todo(contentRef.value, Date.now()))
        contentRef.value = ''
        return
    }
}

export default TodosList