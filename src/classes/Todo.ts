class Todo {
    constructor(
        public content: string, 
        public creationDate = Date.now(),
        public done = false,
    ) {}

    get getDateString(): string {
        const date = new Date(this.creationDate)        
        return `${getDateString(date)}, ${getTimeString(date)}`
    }
}

const getTwoDigitsNumberString = (number: number): string => `${number < 10 ? '0' : ''}${number}`
const getTimeString = (date: Date): string => `${date.getHours()}:${getTwoDigitsNumberString(date.getMinutes())}:${getTwoDigitsNumberString(date.getSeconds())}`
const getDateString = (date: Date): string => String(date.toLocaleDateString("pl-PL"))

export default Todo