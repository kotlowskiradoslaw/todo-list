export const Texts = {
    greetings: "Hello, ",
    createTodoHeader: "Create a todo",
    createTodoSubHeader: "What's on your todo list?",
    todoListHeader: "TODO LIST",
    namePlaceHolder: "Your name here",
    addTodoPlaceHolder: "e.g. get a job",
    addTodoButton: "Add todo",
    addTodoButtonIfItemExists: "Can't add todo - already on the list",
    todoListElementTitle: "created at ",
    deleteButton: "Delete",
} as const

export default Texts