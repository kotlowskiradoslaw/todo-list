#### https://kotlowskiradoslaw.gitlab.io/todo-list
# Todo list - website
A simple _HTML_ website providing a **todo listapplication**  which uses local storage as a database.

## Used technologies:
- Vue 3
- vite
- typescript

## Specification
A user is able to add, delete and edit a todo list items.
##### Adding an item
Using an input and a submit button
##### Deleting an item
Using a button
##### Editing an item
Using an input placed in the list item row

## Source
Source code: [by Tyler Potts](https://github.com/TylerPottsDev/yt-vue-todo-2022)

### Main changes:
* javascript -> typescript
* created classes _Todo_ and _TodosList_
* separated the _template_ part of html from the _script_ part
* created a storage for app texts in _Texts.ts_
* removed cathegories option and its styles
* prevented from adding a todo that already exists in the todos list
  * user is still able to modify an existing todo into the other one without error
